import { $ } from '@core/dom'

export function resizeHandler($root, event) {
  const resizeType = event.target.dataset.resize
  const $resizer = $(event.target)
  const $parent = $resizer.closest('[data-type="resizable"]')
  const coords = $parent.getCoords()
  let value

  document.onmousemove = (e) => {
    if (resizeType === 'col') {
      const delta = e.pageX - coords.right
      value = coords.width + delta
      $resizer.css({ right: `${-delta}px` })
    } else {
      const delta = e.pageY - coords.bottom
      value = coords.height + delta
      $resizer.css({ bottom: `${-delta}px` })
    }
  }

  document.onmouseup = () => {
    if (resizeType === 'col') {
      $parent.css({ width: `${value}px` })
      $resizer.css({ right: 0 })
      $root
          .findAll(`[data-col="${$parent.data.col}"]`)
          .forEach((el) => $(el).css({ width: `${value}px` }))
    } else {
      $parent.css({ height: `${value}px` })
      $resizer.css({ bottom: 0 })
    }

    document.onmousemove = null
    document.onmouseup = null
  }
}
