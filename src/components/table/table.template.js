const CODES = { A: 65, Z: 90 }

function createCell(content, index) {
  return `
    <div class="cell" contenteditable data-col="${index}">${content}</div>
  `
}

function createCol(el, index) {
  return `
    <div class="column" data-type="resizable" data-col="${index}">
      ${el}
      <div class="col-resize" data-resize="col"></div>
    </div>
  `
}

function createRow(content, rowNumber = '') {
  const resizer =
    rowNumber ? `<div class="row-resize" data-resize="row"></div>` : ''

  return `
    <div class="row" data-type="resizable">
      <div class="row-info">
        ${rowNumber}
        ${resizer}
      </div>
      <div class="row-data">${content}</div>
    </div>
  `
}

function toChar(_, index) {
  return String.fromCharCode(CODES.A + index)
}

export function createTable(rowsCount = 15) {
  const colsCount = CODES.Z - CODES.A + 1
  const rows = []
  const cols =
    Array.from({ length: colsCount })
        .fill('')
        .map(toChar)
        .map(createCol)
        .join('')
  const cells =
    Array.from({ length: colsCount })
        .fill('')
        .map(createCell)
        .join('')

  rows.push(createRow(cols))

  Array.from({ length: rowsCount }).forEach((_, index) => {
    rows.push(createRow(cells, index + 1))
  })

  return rows.join('')
}
