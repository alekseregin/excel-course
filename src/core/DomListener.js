import { capitalize } from '@core/utils'

export class DomListener {
  constructor($root, listeners = []) {
    if (!$root) {
      throw new Error('No $root provided for DomListener!')
    }

    this.$root = $root
    this.listeners = listeners
    this.boundedListeners = {}
  }

  initDOMListeners() {
    this.listeners.forEach((listener) => {
      const method = getMethodName(listener)

      if (!this[method]) {
        const { name } = this.constructor

        throw new Error(
            `Method ${method} is not implemented in ${name} Component`
        )
      }

      const boundedListener = this[method].bind(this)

      this.boundedListeners[listener] = boundedListener
      this.$root.on(listener, boundedListener)
    })
  }

  removeDOMListeners() {
    this.listeners.forEach((listener) => {
      this.$root.off(listener, this.boundedListeners[listener])
    })
  }
}

function getMethodName(eventName) {
  return 'on' + capitalize(eventName)
}
